{
  "layers": [
    {
      "name": "Scrolling Contents Layer",
      "bounds": [1178, 1397],
      "contentsOpaque": true,
      "backgroundColor": "#FFFFFF",
      "invalidations": [
        [853, 771, 122, 61],
        [853, 621, 122, 61],
        [853, 471, 122, 61],
        [853, 246, 122, 61],
        [853, 171, 122, 61],
        [640, 771, 122, 61],
        [640, 621, 122, 61],
        [640, 471, 122, 61],
        [640, 246, 122, 61],
        [640, 171, 122, 61],
        [427, 771, 122, 61],
        [427, 621, 122, 61],
        [427, 471, 122, 61],
        [427, 246, 122, 61],
        [427, 171, 122, 61],
        [214, 771, 122, 61],
        [214, 621, 122, 61],
        [214, 471, 122, 61],
        [214, 246, 122, 61],
        [214, 171, 122, 61],
        [897, 846, 78, 61],
        [897, 696, 78, 61],
        [897, 546, 78, 61],
        [897, 396, 78, 61],
        [897, 321, 78, 61],
        [897, 96, 78, 61],
        [684, 846, 78, 61],
        [684, 696, 78, 61],
        [684, 546, 78, 61],
        [684, 396, 78, 61],
        [684, 321, 78, 61],
        [684, 96, 78, 61],
        [471, 846, 78, 61],
        [471, 696, 78, 61],
        [471, 546, 78, 61],
        [471, 396, 78, 61],
        [471, 321, 78, 61],
        [471, 96, 78, 61],
        [258, 846, 78, 61],
        [258, 696, 78, 61],
        [258, 546, 78, 61],
        [258, 396, 78, 61],
        [258, 321, 78, 61],
        [258, 96, 78, 61]
      ]
    },
    {
      "name": "LayoutVideo VIDEO id='video'",
      "bounds": [225, 90],
      "drawsContent": false,
      "transform": 1
    },
    {
      "name": "ContentsLayer for LayoutVideo VIDEO id='video'",
      "bounds": [225, 90],
      "transform": 1
    },
    {
      "name": "LayoutNGFlexibleBox (relative positioned) DIV class='sizing-small phase-ready state-stopped'",
      "bounds": [225, 90],
      "transform": 1
    },
    {
      "name": "Squashing Layer (first squashed layer: LayoutNGBlockFlow (positioned) DIV)",
      "position": [-1, 0],
      "bounds": [226, 90],
      "transform": 1
    },
    {
      "name": "ContentsLayer for Vertical Scrollbar Layer",
      "position": [1178, 0],
      "bounds": [22, 900],
      "contentsOpaque": true
    }
  ],
  "transforms": [
    {
      "id": 1,
      "transform": [
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [23, 1286, 0, 1]
      ]
    }
  ]
}

