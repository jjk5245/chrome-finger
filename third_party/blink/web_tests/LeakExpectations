# tags: [ Android Fuchsia Linux Mac Mac10.12 Mac10.13 Win Win7 Win10 ]
# tags: [ Release Debug ]
# results: [ Timeout Crash Pass Failure Slow Skip ]

# This file is used for tests that only need to be suppressed on the Chromium
# WebKit Leak bots.
# Further documentation:
# https://chromium.googlesource.com/chromium/src/+/master/docs/testing/web_test_expectations.md

###########################################################################
# WARNING: Memory leaks must be fixed asap. Sheriff is expected to revert #
# culprit CLs instead of suppressing the leaks. If you have any question, #
# ask hajimehoshi@ or yuzus@.                                             #
###########################################################################

crbug.com/786995 virtual/threaded/http/tests/devtools/tracing/timeline-style/timeline-style-recalc-all-invalidator-types.js [ Failure Pass ]

# Requests with keepalive specified will be kept alive even when the frame is
# detached, which means leaks reported by the leak detector are by design.
# Ignore them.
crbug.com/755625 external/wpt/beacon/beacon-error.window.html [ Failure ]
crbug.com/651742 external/wpt/content-security-policy/connect-src/connect-src-beacon-allowed.sub.html [ Failure ]

crbug.com/1003224 external/wpt/clipboard-apis/async-write-image-read-image.html [ Failure ]

# These tests crash with the leak detector enabled. Previously, crashes inside
# leak detection were incorrectly hidden with the test passing.
# See https://chromium-review.googlesource.com/c/chromium/src/+/2252943
crbug.com/867532 external/wpt/workers/modules/dedicated-worker-import-data-url.any.worker.html [ Crash ]
crbug.com/867532 external/wpt/workers/modules/dedicated-worker-import.any.worker.html [ Crash ]
crbug.com/867532 http/tests/workers/worker-usecounter.html [ Crash ]

# These tests crash with the leak detector enabled. Previously, crashes inside
# leak detection were incorrectly hidden with the test passing.
# See https://chromium-review.googlesource.com/c/chromium/src/+/2252943
crbug.com/1098106 virtual/off-main-thread-css-paint/http/tests/csspaint/invalidation-border-image.html [ Crash ]
crbug.com/1098106 virtual/off-main-thread-css-paint/http/tests/csspaint/invalidation-background-image.html [ Crash ]
crbug.com/1098106 virtual/off-main-thread-css-paint/http/tests/csspaint/invalidation-content-image.html [ Crash ]

# Random timeout
crbug.com/1151861 [ Linux ] external/wpt/workers/semantics/multiple-workers/003.html [ Timeout Crash ]

# -----------------------------------------------------------------
# Flakily leaks
# -----------------------------------------------------------------
crbug.com/780386 external/wpt/html/dom/reflection-grouping.html [ Failure Pass ]
crbug.com/667560 [ Linux ] http/tests/devtools/console/console-search.js [ Failure Pass ]
crbug.com/835943 [ Linux ] http/tests/appcache/non-html.xhtml [ Failure Pass ]

crbug.com/860117 [ Linux ] editing/pasteboard/drag-drop-iframe-refresh-crash.html [ Pass Failure ]

crbug.com/809609 [ Linux ] editing/pasteboard/drop-file-svg.html [ Pass Failure ]
crbug.com/809609 [ Linux ] editing/inserting/insert_div_with_style.html [ Pass Failure ]

# -----------------------------------------------------------------
# Sheriff 2018-04-23
# -----------------------------------------------------------------
crbug.com/836278 [ Linux ] external/wpt/html/canvas/offscreen/manual/convert-to-blob/offscreencanvas.convert.to.blob.html [ Pass Failure ]

# Moved from TestExpectations (Sheriff 2018-05-31)
crbug.com/848354 [ Linux ] plugins/fullscreen-plugins-dont-reload.html [ Pass Failure ]

# Sheriff 2018-07-10
# Test flaking on Linux Trusty Leak
crbug.com/862029 [ Linux ] http/tests/devtools/tracing/timeline-misc/timeline-window-filter.js [ Pass Failure ]

# Sheriff 2018-08-17
crbug.com/847114 [ Linux ] http/tests/devtools/tracing/decode-resize.js [ Pass Failure ]

# Sheriff 2018-08-29
crbug.com/878724 [ Linux ] editing/selection/modify_extend/extend_by_character.html [ Pass Failure ]
crbug.com/878724 [ Linux ] editing/selection/modify_move/move_right_word_09_ltr_multi_line.html [ Pass Failure ]

crbug.com/733494 [ Linux ] media/autoplay/document-user-activation.html [ Pass Failure ]

# Sheriff 2019-01-07
crbug.com/919497 [ Linux ] virtual/threaded/http/tests/devtools/tracing/timeline-gpu-tasks.js [ Pass Failure ]

# Sheriff 2019-02-21
crbug.com/934144 [ Linux ] http/tests/devtools/tracing/timeline-misc/timeline-flame-chart-automatically-size-window.js [ Pass Failure ]

# Sheriff 2019-08-21
crbug.com/996235 [ Linux ] media/controls/doubletap-to-jump-backwards-at-start.html [ Pass Failure ]

crbug.com/1000512 [ Linux ] http/tests/devtools/a11y-axe-core/application-panel/app-manifest-view-a11y-test.js [ Failure Pass ]
crbug.com/1000512 [ Linux ] http/tests/devtools/a11y-axe-core/audits-start-view-a11y-test.js [ Failure Pass ]
crbug.com/1000512 [ Linux ] http/tests/devtools/a11y-axe-core/basic-a11y-test.js [ Failure Pass ]
crbug.com/1000512 [ Linux ] http/tests/devtools/a11y-axe-core/console/console-a11y-test.js [ Failure Pass ]
crbug.com/1000512 [ Linux ] http/tests/devtools/a11y-axe-core/elements/event-listeners-a11y-test.js [ Failure Pass ]
crbug.com/1000512 [ Linux ] http/tests/devtools/a11y-axe-core/memory/heap-profiler-a11y-test.js [ Failure Pass ]
crbug.com/1000512 [ Linux ] http/tests/devtools/a11y-axe-core/network/network-condition-a11y-test.js [ Failure Pass ]
crbug.com/1000512 [ Linux ] http/tests/devtools/a11y-axe-core/security/security-origin-a11y-test.js [ Failure Pass ]
crbug.com/1046784 [ Linux ] http/tests/devtools/a11y-axe-core/settings/menu-a11y-test.js [ Failure Pass ]
crbug.com/1046784 [ Linux ] http/tests/devtools/a11y-axe-core/settings/shortcuts-a11y-test.js [ Failure Pass ]
crbug.com/1000512 [ Linux ] http/tests/devtools/a11y-axe-core/sources/source-navigator-filesystem-a11y-test.js [ Failure Pass ]
crbug.com/1000512 [ Linux ] http/tests/devtools/a11y-axe-core/sources/source-navigator-network-a11y-test.js [ Failure Pass ]
crbug.com/1000512 [ Linux ] http/tests/devtools/a11y-axe-core/sources/sources-editor-pane-a11y-test.js [ Failure Pass ]

# Only times out on the leak bots.
crbug.com/998399 [ Linux ] virtual/plz-dedicated-worker/external/wpt/service-workers/service-worker/worker-interception.https.html [ Pass Timeout ]

crbug.com/769885 [ Linux ] virtual/android/fullscreen/full-screen-frameset.html [ Failure ]
crbug.com/769885 [ Linux ] virtual/android/fullscreen/video-scrolled-iframe.html [ Failure ]

# Sheriff 2019-09-04
crbug.com/1000768 [ Linux ] external/wpt/web-locks/idlharness.tentative.https.any.html [ Pass Failure ]
crbug.com/1000768 [ Linux ] external/wpt/geolocation-API/idlharness.window.html [ Pass Failure ]
crbug.com/1000768 [ Linux ] external/wpt/web-share/idlharness.https.window.html [ Pass Failure ]
crbug.com/1000768 [ Linux ] external/wpt/storage/idlharness.https.any.html [ Pass Failure ]

# Sheriff 2019-11-29
crbug.com/1029417 [ Linux ] external/wpt/web-nfc/NDEFReader_scan_filter.https.html [ Failure ]

# Sheriff 2019-12-30
crbug.com/1038388 [ Linux ] http/tests/devtools/tracing/timeline-time/timeline-time.js [ Pass Failure ]

# Sheriff 2020-04-03
crbug.com/1067650 [ Linux ] http/tests/devtools/tracing/timeline-misc/timeline-load-event.js [ Pass Failure ]

# Sheriff 2020-04-06
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-classic.http-rp/no-referrer/worker-module.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-classic.http-rp/origin/worker-classic.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-classic.http-rp/strict-origin/worker-classic.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-classic.http-rp/strict-origin/worker-module.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-classic.http-rp/unsafe-url/worker-module.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-module.http-rp/no-referrer-when-downgrade/worker-module.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-module.http-rp/no-referrer/worker-classic.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-module.http-rp/no-referrer/worker-module.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-module.http-rp/origin-when-cross-origin/worker-module.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-module.http-rp/same-origin/worker-module.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-module.http-rp/strict-origin-when-cross-origin/worker-module.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-module.http-rp/strict-origin/worker-module.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-classic.http-rp/same-origin/worker-classic.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] virtual/plz-dedicated-worker/external/wpt/referrer-policy/gen/worker-classic.http-rp/strict-origin-when-cross-origin/worker-classic.http.html [ Pass Timeout ]

# Sheriff 2020-04-20
crbug.com/1071849 [ Linux ] http/tests/devtools/isolated-code-cache/same-origin-module-test.js [ Pass Failure ]

# Sheriff 2020-04-23
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-module.http-rp/same-origin/worker-classic.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-module.http-rp/origin/worker-module.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-module.http-rp/unsafe-url/worker-module.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-classic.http-rp/origin/worker-module.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-classic.http-rp/no-referrer-when-downgrade/worker-module.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-classic.http-rp/unset/worker-module.http.html [ Pass Timeout ]

# Sheriff 2020-05-06
crbug.com/1078769 [ Linux ] external/wpt/wasm/jsapi/idlharness.any.html [ Pass Failure ]

# Sheriff 2020-06-24
# Also crbug.com/867532
crbug.com/1098782 [ Linux ] virtual/plz-dedicated-worker/external/wpt/workers/modules/dedicated-worker-import.any.worker.html [ Pass Timeout Crash ]

# Sheriff 2020-07-14
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-module.http-rp/strict-origin-when-cross-origin/worker-classic.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-module.http-rp/unsafe-url/worker-classic.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-module.http-rp/unset/worker-module.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-module.http-rp/no-referrer-when-downgrade/worker-classic.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-module.http-rp/origin-when-cross-origin/worker-classic.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-module.http-rp/strict-origin/worker-classic.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-classic.http-rp/strict-origin-when-cross-origin/worker-module.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] virtual/plz-dedicated-worker/external/wpt/referrer-policy/gen/worker-classic.http-rp/unset/worker-classic.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-module.http-rp/origin/worker-classic.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-classic.http-rp/same-origin/worker-module.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-classic.http-rp/origin-when-cross-origin/worker-classic.http.html [ Pass Timeout ]

# Sheriff 2020-07-15
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-classic.http-rp/origin-when-cross-origin/worker-module.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-classic.http-rp/no-referrer/worker-classic.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-classic.http-rp/no-referrer-when-downgrade/worker-classic.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-classic.http-rp/unsafe-url/worker-classic.http.html [ Pass Timeout ]
crbug.com/1068175 [ Linux ] external/wpt/referrer-policy/gen/worker-module.http-rp/unset/worker-classic.http.html [ Pass Timeout ]

# This test triggers existing leaky behavior, but this test also catches
# a prior crash.
crbug.com/1103082 [ Linux ] fast/forms/select/select-change-layout-object-crash.html [ Failure ]

# Sheriff 2020-10-08
crbug.com/1136690 [ Linux ] http/tests/inspector-protocol/service-worker/service-worker-fetch-async-stacks.js [ Pass Timeout ]

# Sheriff 20201-02-16
crbug.com/1178723 [ Linux ] http/tests/devtools/network/ping-response.js [ Pass Failure ]
crbug.com/1178725 [ Linux ] http/tests/devtools/sources/snippet-overrides.js [ Pass Failure ]

# Sheriff 2021-02-18
crbug.com/1179722 [ Linux ] http/tests/devtools/console/console-string-format.js [ Pass Failure ]
crbug.com/1179784 [ Linux ] http/tests/devtools/indexeddb/database-data.js [ Pass Failure ]
crbug.com/1179784 [ Linux ] http/tests/devtools/indexeddb/transaction-promise-console.js [ Pass Failure ]

# Sheriff 2021-02-18
crbug.com/1180251 [ Linux ] virtual/wheel-event-regions/fast/events/platform-wheelevent-paging-xy-in-scrolling-div.html [ Pass Failure ]
crbug.com/1180251 [ Linux ] http/tests/devtools/sources/debugger-pause/debugger-pause-on-promise-rejection.js [ Pass Failure ]
crbug.com/1180251 [ Linux ] http/tests/devtools/sources/debugger-async/async-await/async-pause-on-exception.js [ Pass Failure ]

# Sheriff 2021-03-02
crbug.com/1183788 [ Linux ] external/wpt/pointerevents/pointerevent_fractional_coordinates.html?touch [ Pass Failure ]
crbug.com/1183788 [ Linux ] external/wpt/webrtc/protocol/handover.html [ Pass Failure ]
crbug.com/1183788 [ Linux ] external/wpt/webrtc/RTCPeerConnection-restartIce.https.html [ Pass Failure ]
crbug.com/1183788 [ Linux ] external/wpt/webrtc/RTCPeerConnection-onnegotiationneeded.html [ Pass Failure ]
crbug.com/1183788 [ Linux ] external/wpt/webrtc/RTCRtpTransceiver.https.html [ Pass Failure ]

# These tests are flaky on the Leak bots but seem reliable elsewhere
crbug.com/1164553 [ Linux ] http/tests/devtools/a11y-axe-core/console/console-error-a11y-test.js [ Pass Failure ]
crbug.com/1164553 [ Linux ] http/tests/devtools/console/console-external-array.js [ Pass Failure ]
crbug.com/1164553 [ Linux ] http/tests/devtools/profiler/heap-snapshot-summary-search-by-id.js [ Pass Failure ]
crbug.com/1164553 [ Linux ] http/tests/devtools/runtime/runtime-es6-setSymbolPropertyValue.js [ Pass Failure ]

###########################################################################
# WARNING: Memory leaks must be fixed asap. Sheriff is expected to revert #
# culprit CLs instead of suppressing the leaks. If you have any question, #
# ask hajimehoshi@ or yuzus@.                                             #
###########################################################################
