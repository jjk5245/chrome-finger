This is a testharness.js-based test.
PASS Main
PASS image same-origin + undefined
PASS image same-origin + anonymous
PASS image same-origin + use-credentials
FAIL image cross-origin + undefined assert_equals: coep:credentialless =>  expected (undefined) undefined but got (string) "cross_origin"
PASS image cross-origin + anonymous
PASS image cross-origin + use-credentials
Harness: the test ran to completion.

