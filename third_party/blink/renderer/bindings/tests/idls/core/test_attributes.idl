// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// https://heycam.github.io/webidl/#dfn-attribute-getter
// https://heycam.github.io/webidl/#dfn-attribute-setter

// This interface tests unique behaviors of getters and setters of specific
// types, e.g. returning Promise<T>, having [LegacyLenientThis] or
// [LegacyLenientSetter].

interface TestAttributes {
    // [LegacyLenientThis] causes some V8 type checks to be bypassed; they are
    // done on the Blink side instead and access exceptions just return
    // undefined.
    [LegacyLenientThis] readonly attribute long lenientThisLongAttribute;

    // Promise types must turn exceptions into promise rejections.
    readonly attribute Promise<DOMString> stringPromiseAttribute;

    // Mixing both should cause [LegacyLenientThis] to return without raising an
    // exception if type checking fails.
    [LegacyLenientThis] readonly attribute Promise<DOMString> lenientThisStringPromiseAttribute;

    // [RaisesException] also creates an ExceptionState instance. Make sure it
    // is not declared twice.
    [RaisesException] readonly attribute Promise<short> raisesExceptionShortPromiseAttribute;

    // [LegacyLenientSetter] indicates a no-op setter will be generated for the
    // readonly attribute's accessor property. This avoids throwing an exception
    // in strict mode.
    [LegacyLenientSetter] readonly attribute boolean lenientSetterBoolAttribute;

    // Regular attribute with no special rules.
    readonly attribute float floatAttribute;
};
