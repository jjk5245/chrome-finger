// Copyright 2021 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// TODO(crbug/1179698): use the real extraction script.
function extractProductsInCart() {
  return [
    {
      'imageUrl': 'https://example.com/image.jpg',
      'title': ''
    }
  ];
}
extractProductsInCart();
