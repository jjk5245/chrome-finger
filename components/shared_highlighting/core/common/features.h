// Copyright 2021 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef COMPONENTS_SHARED_HIGHLIGHTING_CORE_COMMON_FEATURES_H_
#define COMPONENTS_SHARED_HIGHLIGHTING_CORE_COMMON_FEATURES_H_

#include "base/feature_list.h"
#include "build/build_config.h"

namespace features {
extern const base::Feature kPreemptiveLinkToTextGeneration;
}

#endif  // COMPONENTS_SHARED_HIGHLIGHTING_CORE_COMMON_FEATURES_H_