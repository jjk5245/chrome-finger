AXWebArea
++AXTable AXRowHeaderUIElements=[:4, :11]
++++AXRow AXIndex=0
++++++AXCell AXColumnIndexRange={len: 1, loc: 0} AXRowIndexRange={len: 1, loc: 0}
++++++++AXStaticText AXValue='Browser'
++++++AXCell AXColumnIndexRange={len: 1, loc: 1} AXRowHeaderUIElements=[:4] AXRowIndexRange={len: 1, loc: 0}
++++++++AXStaticText AXValue='Chrome'
++++++AXCell AXColumnIndexRange={len: 1, loc: 2} AXRowHeaderUIElements=[:4] AXRowIndexRange={len: 1, loc: 0}
++++++++AXStaticText AXValue='Safari'
++++AXRow AXIndex=1
++++++AXCell AXColumnIndexRange={len: 1, loc: 0} AXRowIndexRange={len: 1, loc: 1}
++++++++AXStaticText AXValue='Rendering Engine'
++++++AXCell AXColumnIndexRange={len: 1, loc: 1} AXRowHeaderUIElements=[:11] AXRowIndexRange={len: 1, loc: 1}
++++++++AXStaticText AXValue='Blink'
++++++AXCell AXColumnIndexRange={len: 1, loc: 2} AXRowHeaderUIElements=[:11] AXRowIndexRange={len: 1, loc: 1}
++++++++AXStaticText AXValue='WebKit'
