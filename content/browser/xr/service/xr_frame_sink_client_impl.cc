// Copyright 2021 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/browser/xr/service/xr_frame_sink_client_impl.h"

#include <memory>

namespace content {
XrFrameSinkClientImpl::XrFrameSinkClientImpl() = default;
XrFrameSinkClientImpl::~XrFrameSinkClientImpl() = default;
}  // namespace content
